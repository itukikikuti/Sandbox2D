﻿using UnityEngine;
using System;

public class Player : MonoBehaviour
{
    Mode mode;

    void Start()
    {
        mode = new Mode(GetComponent<Rigidbody2D>());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            mode = new Mode(GetComponent<Rigidbody2D>());

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            //mode = new HighSpeedMode(GetComponent<Rigidbody2D>());
            mode = (Mode)Activator.CreateInstance(Type.GetType("HighSpeedMode"), args: GetComponent<Rigidbody2D>());
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
            mode = new HighJumpMode(GetComponent<Rigidbody2D>());

        mode.Move(Input.GetAxis("Horizontal"));

        if (Input.GetButtonDown("Jump"))
            mode.Jump();
    }
}
