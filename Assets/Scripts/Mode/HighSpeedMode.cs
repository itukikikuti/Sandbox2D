﻿using UnityEngine;

public class HighSpeedMode : Mode
{
    public HighSpeedMode(Rigidbody2D rigidbody) : base(rigidbody)
    {
    }

    public override void Move(float speed)
    {
        rigidbody.velocity += new Vector2(speed * 2f, 0f);
        rigidbody.velocity *= new Vector2(0.7f, 1f);
    }
}
