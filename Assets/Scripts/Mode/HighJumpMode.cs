﻿using UnityEngine;

public class HighJumpMode : Mode
{
    public HighJumpMode(Rigidbody2D rigidbody) : base(rigidbody)
    {
    }

    public override void Jump()
    {
        rigidbody.velocity += new Vector2(0f, 10f);
    }
}
