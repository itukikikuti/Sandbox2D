﻿using UnityEngine;

public class Mode
{
    protected Rigidbody2D rigidbody;

    public Mode(Rigidbody2D rigidbody)
    {
        this.rigidbody = rigidbody;
        Debug.Log(GetType());
    }

    public virtual void Move(float speed)
    {
        rigidbody.velocity += new Vector2(speed, 0f);
        rigidbody.velocity *= new Vector2(0.7f, 1f);
    }

    public virtual void Jump()
    {
        rigidbody.velocity += new Vector2(0f, 7f);
    }
}
